package com.thetitans.tarotapplication;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.database.Cursor;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.thetitans.tarotapplication.data.TarotCardContract.TarotCardEntry;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.*;

public class CardDetailActivity extends AppCompatActivity {

    private final static String TAG = CardDetailActivity.class.getSimpleName();
    private final static Random random = new Random();
    private final static int[] number1 = {
            R.string.one, R.string.two, R.string.three
    };
    private final static int[] number2 = {
            R.string.four, R.string.five,
            R.string.six, R.string.seven
    };
    private final static int[] number3 = {
            R.string.eight, R.string.nine, R.string.zero
    };

    private final static int[] color = {
            R.string.white, R.string.black, R.string.red,
            R.string.yellow, R.string.green, R.string.blue,
            R.string.light_blue, R.string.pink, R.string.purple
    };

    private final static int[] day = {
            R.string.sunday, R.string.monday, R.string.tuesday,
            R.string.wednesday, R.string.thursday, R.string.friday,
            R.string.saturday, R.string.friday, R.string.saturday, R.string.sunday
    };
    private ImageView imgView;
    private TextView cardName, categoryName, cardDetailResult, luckyNumber, luckyColor, luckyDay;
    private Button shareButton;

    String card_name, card_mm_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        String category = bundle.getString("CategoryResult");
        String card = bundle.getString("CooseCard");
        int cardimg = bundle.getInt("ChooseCardImg");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_detail);
        Log.e(TAG, "Card Result : " + card);
        imgView = (ImageView) findViewById(R.id.cardImageResult);
        cardName = (TextView) findViewById(R.id.cardName);
        categoryName = (TextView) findViewById(R.id.label_category);
        cardDetailResult = (TextView) findViewById(R.id.details_info);
        luckyNumber = (TextView) findViewById(R.id.lucky_number);
        luckyColor = (TextView) findViewById(R.id.lucky_color);
        luckyDay = (TextView) findViewById(R.id.lucky_day);

        Cursor c = null;

        try {
            c = getApplicationContext().getContentResolver().query(
                    TarotCardEntry.CONTENT_URI,
                    null,
                    TarotCardEntry.COLUMN_CARD_NAME + " LIKE '%" + card + "%'",
                    null,
                    null
            );
            Log.e(TAG, "Cursor count " + c.getCount());

            if(c != null && c.moveToFirst()) {
                card_name = c.getString(c.getColumnIndex(TarotCardEntry.COLUMN_CARD_NAME));
                card_mm_name = c.getString(c.getColumnIndex(TarotCardEntry.COLUMN_CARD_MM_NAME));
                String business = c.getString(c.getColumnIndex(TarotCardEntry.COLUMN_BUSINESS));
                String money = c.getString(c.getColumnIndex(TarotCardEntry.COLUMN_MONEY));
                String education = c.getString(c.getColumnIndex(TarotCardEntry.COLUMN_EDUCATION));
                String health = c.getString(c.getColumnIndex(TarotCardEntry.COLUMN_HEALTH));
                String love = c.getString(c.getColumnIndex(TarotCardEntry.COLUMN_LOVE));
                String random = c.getString(c.getColumnIndex(TarotCardEntry.COLUMN_RANDOM));

                Log.e(TAG, "card name : " + card_name);

                imgView.setImageResource(cardimg);
                cardName.setText(card_name + " - " + card_mm_name);

                loadCategoryName(category);

                if (category.equalsIgnoreCase("business"))
                    cardDetailResult.setText(business);
                if (category.equalsIgnoreCase("money"))
                    cardDetailResult.setText(money);
                if (category.equalsIgnoreCase("education"))
                    cardDetailResult.setText(education);
                if (category.equalsIgnoreCase("health"))
                    cardDetailResult.setText(health);
                if (category.equalsIgnoreCase("love"))
                    cardDetailResult.setText(love);
                if (category.equalsIgnoreCase("random"))
                    cardDetailResult.setText(random);
            } else {
                Log.e(TAG, "Retrieve Fail!!!!");
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        imgView.setImageResource(cardimg);

        //String randomLuckyNumbers = randomNumber(0,9) + "," + randomNumber(0,9) + "," + randomNumber(0,9);
        String randomLuckyNumbers = randomNumber1() + ", " + randomNumber2() + ", " + randomNumber3();
        String randomLuckyColors = randomColor();
        String randomLuckyDays = randomDay();

        luckyNumber.setText(randomLuckyNumbers);
        luckyColor.setText(randomLuckyColors);
        luckyDay.setText(randomLuckyDays);

        Log.d(TAG, "Random Number : " + randomLuckyNumbers
                + "\nRandom Color : " + randomLuckyColors
                + "\nRandom Day : " + randomLuckyDays);

        //Initialize facebook API
        FacebookSdk.sdkInitialize(getApplicationContext());

        // Add code to print out the key hash
        /*try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.thetitans.tarotapplication",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }*/
    }

    private void loadCategoryName(String category) {
        if (category.equalsIgnoreCase("business"))
            categoryName.setText(R.string.category_business);
        if (category.equalsIgnoreCase("money"))
            categoryName.setText(R.string.category_money);
        if (category.equalsIgnoreCase("education"))
            categoryName.setText(R.string.category_education);
        if (category.equalsIgnoreCase("health"))
            categoryName.setText(R.string.category_health);
        if (category.equalsIgnoreCase("love"))
            categoryName.setText(R.string.category_love);
        if (category.equalsIgnoreCase("random"))
            categoryName.setText(R.string.category_random);
    }

    private String randomNumber1() {
        int rdmNumber = random.nextInt(number1.length);
        String value = getResources().getString(number1[rdmNumber]);
        return value;
    }

    private String randomNumber2() {
        int rdmNumber = random.nextInt(number2.length);
        String value = getResources().getString(number2[rdmNumber]);
        return value;
    }

    private String randomNumber3() {
        int rdmNumber = random.nextInt(number3.length);
        String value = getResources().getString(number3[rdmNumber]);
        return value;
    }

    private String randomColor() {
        int rdmColor = random.nextInt(color.length);
        String value = getResources().getString(color[rdmColor]);
        return value;
    }

    private String randomDay() {
        int rdmDay = random.nextInt(day.length);
        String value = getResources().getString(day[rdmDay]);
        return value;
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(CardDetailActivity.this, CategoryListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    CallbackManager callbackManager;
    private LoginManager manager;
    //List<String> permissionNeeds = Arrays.asList("public_profile", "user_friends");

    public void onActionShareFacebook(View v) {

        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.the_tarot_bg_white_192))
                .setUserGenerated(true)
                .build();

        // Create an object
        ShareOpenGraphObject object = new ShareOpenGraphObject.Builder()
                .putString("og:type", "books.book")
                .putString("og:title", cardName.getText().toString())
                .putString("og:description", cardDetailResult.getText().toString())
                .putString("books:isbn", "0-553-57340-3")
                .build();

        // Create an action
        ShareOpenGraphAction action = new ShareOpenGraphAction.Builder()
                .setActionType("books.reads")
                .putObject("book", object)
                .putPhoto("image", photo)
                .build();

        // Create the content
        ShareOpenGraphContent content = new ShareOpenGraphContent.Builder()
                .setPreviewPropertyName("book")
                .setAction(action)
                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.thetitans.tarotapplication"))
                .build();

        ShareDialog.show(this, content);
    }

    private void publishLink(){

        /*ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentTitle("The Tarot")
                .setContentDescription(cardDetailResult.getText().toString())
                .build();*/

        Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.the_tarot_bg_white_192);

        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        ShareApi.share(content, null);

    }

    private void sharePhotoToFacebook(){

        /*Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption("Give me my codez or I will ... you know, do that thing you don't like!")
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        ShareApi.share(content, null);*/

        /*ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.thetitans.tarotapplication"))
                .setContentTitle(cardName.getText().toString())
                .setContentDescription(cardDetailResult.getText().toString())
                .build();

        ShareApi.share(content, null);*/

    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }*/

}
