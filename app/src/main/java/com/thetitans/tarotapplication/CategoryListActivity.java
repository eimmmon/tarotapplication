package com.thetitans.tarotapplication;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.Random;

public class CategoryListActivity extends AppCompatActivity /*implements View.OnClickListener*/ {

    private static String TAG = CategoryListActivity.class.getSimpleName();
    Context mContext;
    private final static Random random = new Random();
    String zodiacResult = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle myBundle = getIntent().getExtras();
        if(myBundle != null) {
            if(myBundle.containsKey("Zodiac")) {
                zodiacResult = myBundle.getString("Zodiac");
                Log.i("Activity2 Log", "Zodiac Result:"+ zodiacResult);
            }
        }
        //String res = myBundle.getString("Zodiac");
        //Log.i(TAG, "Result : " + res);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar != null){
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
        }

        setContentView(R.layout.activity_category_list);
    }

    public void onActionBusiness(View view) {
        String value = "Business";
        clickCategory(value, zodiacResult);
    }

    public void onActionMoney(View view) {
        String value = "Money";
        clickCategory(value, zodiacResult);
    }

    public void onActionEducation(View view) {
        String value = "Education";
        clickCategory(value, zodiacResult);
    }

    public void onActionHealth(View view) {
        String value = "Health";
        clickCategory(value, zodiacResult);
    }

    public void onActionLove(View view) {
        String value = "Love";
        clickCategory(value, zodiacResult);
    }

    public void onActionRandom(View view) {
        String value = "Random";
        clickCategory(value, zodiacResult);
    }

    public void clickCategory(String value, String zodiacResult) {
        Intent myIntent = new Intent(CategoryListActivity.this, CardActivity.class);
        Bundle myBundle = new Bundle();
        myBundle.putString("Category", value);
        myBundle.putString("ZodiacResult", zodiacResult);
        myIntent.putExtras(myBundle);
        startActivity(myIntent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CategoryListActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
