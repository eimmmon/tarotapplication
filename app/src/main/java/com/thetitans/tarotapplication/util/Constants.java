package com.thetitans.tarotapplication.util;

/**
 * Created by CS on 05-Feb-16.
 */
public interface Constants {

    public static final String TYPEFACE_ROBOTO_BOLD = "Roboto-Bold";
    public static final String TYPEFACE_ROBOTO_REGULAR = "Roboto-Regular";
    public static final String TYPEFACE_ZAWGYI_ONE = "Zawgyi-One";
    public static final String TYPEFACE_ZAWGYI_ONE_BOLD = "Zawgyi-One-Bold";
}
