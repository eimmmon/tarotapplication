package com.thetitans.tarotapplication.model;

/**
 * Created by ei on 10-Feb-16.
 */
public class CardData {

    private String id;
    private String name;
    private String mmname;
    private String business;
    private String money;
    private String education;
    private String health;
    private String love;
    private String random;

    public CardData() {
    }

    public CardData(String id, String name, String mmname, String business, String money, String education, String health, String love, String random) {
        this.id = id;
        this.name = name;
        this.mmname = mmname;
        this.business = business;
        this.money = money;
        this.education = education;
        this.health = health;
        this.love = love;
        this.random = random;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMmname() {
        return mmname;
    }

    public void setMmname(String mmname) {
        this.mmname = mmname;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getLove() {
        return love;
    }

    public void setLove(String love) {
        this.love = love;
    }

    public String getRandom() {
        return random;
    }

    public void setRandom(String random) {
        this.random = random;
    }
}
