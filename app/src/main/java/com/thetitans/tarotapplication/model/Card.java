package com.thetitans.tarotapplication.model;

/**
 * Created by CS on 10-Feb-16.
 */
public class Card {
    private String cardName;
    private int cardImg;

    public Card(String cardName, int cardImg) {
        this.cardName = cardName;
        this.cardImg = cardImg;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public int getCardImg() {
        return cardImg;
    }

    public void setCardImg(int cardImg) {
        this.cardImg = cardImg;
    }
}
