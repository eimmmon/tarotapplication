package com.thetitans.tarotapplication.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.thetitans.tarotapplication.util.Constants;
import com.thetitans.tarotapplication.util.Typefaces;

/**
 * Created by CS on 08-Feb-16.
 */
public class ZawgyiOneBoldTextView extends TextView implements Constants {

    public ZawgyiOneBoldTextView(Context context) {
        super(context);
        setTypeface(Typefaces.get(context, TYPEFACE_ZAWGYI_ONE), Typeface.BOLD);
    }

    public ZawgyiOneBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(Typefaces.get(context, TYPEFACE_ZAWGYI_ONE), Typeface.BOLD);
    }

    public ZawgyiOneBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Typefaces.get(context, TYPEFACE_ZAWGYI_ONE), Typeface.BOLD);
    }
}
