package com.thetitans.tarotapplication.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.thetitans.tarotapplication.util.Constants;
import com.thetitans.tarotapplication.util.Typefaces;

/**
 * Created by ei on 05-Feb-16.
 */
public class RobotoBoldTextView extends TextView implements Constants {

    public RobotoBoldTextView(Context context)
    {
        super(context);
        setTypeface(Typefaces.get(context, TYPEFACE_ROBOTO_BOLD));
    }

    public RobotoBoldTextView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        setTypeface(Typefaces.get(context, TYPEFACE_ROBOTO_BOLD));
    }

    public RobotoBoldTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setTypeface(Typefaces.get(context, TYPEFACE_ROBOTO_BOLD));
    }
}
