package com.thetitans.tarotapplication.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import com.thetitans.tarotapplication.util.SelectionBuilder;

/**
 * Created by ei on 11-Feb-16.
 */
public class TarotCardProvider extends ContentProvider {

    private static final String TAG = TarotCardProvider.class.getSimpleName();

    private TarotCardDBHelper mDatabaseHelper;

    private static final int TAROT_CARD = 1000;

    private final UriMatcher matcher = buildMatchUri();

    @Override
    public boolean onCreate() {
        mDatabaseHelper = new TarotCardDBHelper(getContext());
        mDatabaseHelper.getWritableDatabase();
        Log.d(TAG, "database created!");
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        switch (matcher.match(uri)) {
            case TAROT_CARD: {
                retCursor = mDatabaseHelper.getReadableDatabase().query(
                        TarotCardContract.TarotCardEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {

        int matchValue = matcher.match(uri);

        switch (matchValue) {

            case TAROT_CARD :
                return TarotCardContract.TarotCardEntry.CONTENT_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown uri : "+uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final int match = matcher.match(uri);
        Uri returnUri;
        Log.d(TAG, "insert() called with: " + "uri = [" + uri + "], values = [" + contentValues + "]");

        switch (match) {
            case TAROT_CARD : {
                long id = db.insert(TarotCardContract.TarotCardEntry.TABLE_NAME, null, contentValues);
                if(id > 0) {
                    returnUri = TarotCardContract.TarotCardEntry.buildTarotCardUri(contentValues.getAsString(TarotCardContract.TarotCardEntry.COLUMN_CARD_ID));
                } else
                    throw new SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown Uri : "+uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }

    private static UriMatcher buildMatchUri() {
        Log.d("URI Matcher ", " Build UriMatcher");

        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        final String CONTENT_AUTHORITY = TarotCardContract.CONTENT_AUTHORITY;

        matcher.addURI(CONTENT_AUTHORITY, "tarot_card", TAROT_CARD);

        return matcher;
    }

    private SelectionBuilder buiderSelections(Uri uri, int match_value) {
        SelectionBuilder selectionBuilder = new SelectionBuilder();

        switch (match_value) {

            case TAROT_CARD :
                return selectionBuilder.table(TarotCardContract.TarotCardEntry.TABLE_NAME);

            default:
                throw new UnsupportedOperationException("Unknown Uri : "+uri);
        }
    }

    /*private static final UriMatcher sUriMatcher = buildUriMatcher();
    private TarotCardDBHelper mOpenHelper;
    private static final int TAROT_CARD = 100;

    SQLiteDatabase tarotsDB;

    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = TarotCardContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, TarotCardContract.PATH_TAROT_CARD, TAROT_CARD);
        //matcher.addURI(authority, TarotCardContract.PATH_TAROT_CARD + "/#", TAROT_CARD_WITH_NAME);
        return matcher;
    }

    @Override
    public boolean onCreate() {

        mOpenHelper = new TarotCardDBHelper(getContext());
        mOpenHelper.getWritableDatabase();
        //return (tarotsDB == null)? false:true;
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor = null;
        switch (sUriMatcher.match(uri)) {
            case TAROT_CARD:
                retCursor = mOpenHelper.getReadableDatabase().query(
                        TarotCardContract.TarotCardEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            *//*case TAROT_CARD_WITH_NAME:
                retCursor = null;
                break;*//*
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case TAROT_CARD :
                return TarotCardContract.TarotCardEntry.CONTENT_TYPE;

            *//*case TAROT_CARD_WITH_NAME :
                return TarotCardContract.TarotCardEntry.CONTENT_ITEM_TYPE;*//*

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case TAROT_CARD: {
                long _id = db.insert(TarotCardContract.TarotCardEntry.TABLE_NAME, null, contentValues);
                if (_id > 0) {
                    Log.d(TAG, "insert tarot_cards id : " + _id);

                    returnUri = TarotCardContract.TarotCardEntry.builtTarotCardUri(TarotCardContract.TarotCardEntry.COLUMN_CARD_ID);
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                else
                    throw new android.database.SQLException("Fail to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case TAROT_CARD:
                rowsUpdated = db.update(TarotCardContract.TarotCardEntry.TABLE_NAME, contentValues, selection, selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if(rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case TAROT_CARD:
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(TarotCardContract.TarotCardEntry.TABLE_NAME, null, value);
                        if(_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }
    }*/
}
