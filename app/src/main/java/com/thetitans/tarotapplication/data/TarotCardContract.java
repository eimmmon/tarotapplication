package com.thetitans.tarotapplication.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by ei on 11-Feb-16.
 */
public class TarotCardContract {

    public static final String CONTENT_AUTHORITY = "com.thetitans.tarotapplication";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String TAROT_CARD_PATH = "tarot_card";

    public static final class TarotCardEntry {

        public static Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(TAROT_CARD_PATH).build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.thetitans.tarotapplication.tarot_card";

        public static Uri buildTarotCardUri(String _id) {
            return CONTENT_URI.buildUpon().appendPath(_id).build();
        }

        //Table Name
        public static final String TABLE_NAME = "tarot_card";
        public static final String COLUMN_CARD_ID = "id";
        public static final String COLUMN_CARD_NAME = "name";
        public static final String COLUMN_CARD_MM_NAME = "mmname";
        public static final String COLUMN_BUSINESS = "business";
        public static final String COLUMN_MONEY = "money";
        public static final String COLUMN_EDUCATION = "education";
        public static final String COLUMN_HEALTH = "health";
        public static final String COLUMN_LOVE = "love";
        public static final String COLUMN_RANDOM = "random";
    }
}
