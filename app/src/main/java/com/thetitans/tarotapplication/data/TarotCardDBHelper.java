package com.thetitans.tarotapplication.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import com.thetitans.tarotapplication.data.TarotCardContract.TarotCardEntry;
import android.util.Log;
;
/**
 * Created by CS on 11-Feb-16.
 */
public class TarotCardDBHelper extends SQLiteOpenHelper {

    private static final String TAG = TarotCardDBHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "tarotcards_db";

    public TarotCardDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String SQL_CREATE_TAROTCARD_TABLE="Create Table If Not Exists "+TarotCardEntry.TABLE_NAME+" ("+
                BaseColumns._ID+" Integer Primary Key Autoincrement ,"+
                TarotCardEntry.COLUMN_CARD_ID+" Text Not Null,"+
                TarotCardEntry.COLUMN_CARD_NAME+" Text Not Null,"+
                TarotCardEntry.COLUMN_CARD_MM_NAME+" Text Not Null,"+
                TarotCardEntry.COLUMN_BUSINESS + " TEXT NOT NULL, " +
                TarotCardEntry.COLUMN_MONEY  + " TEXT NOT NULL, " +
                TarotCardEntry.COLUMN_EDUCATION + " TEXT NOT NULL, " +
                TarotCardEntry.COLUMN_HEALTH + " TEXT NOT NULL, " +
                TarotCardEntry.COLUMN_LOVE + " TEXT NOT NULL, " +
                TarotCardEntry.COLUMN_RANDOM + " TEXT NOT NULL, " +
                " Unique ( "+ TarotCardEntry.COLUMN_CARD_NAME+" ) ON CONFLICT REPLACE);";

        sqLiteDatabase.execSQL(SQL_CREATE_TAROTCARD_TABLE);
        Log.d(TAG, "Table Created!");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TarotCardContract.TarotCardEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void deleteDatabase(Context context) {
        context.deleteDatabase(DATABASE_NAME);
    }
}
