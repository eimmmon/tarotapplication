package com.thetitans.tarotapplication;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import com.thetitans.tarotapplication.data.TarotCardContract;
import com.thetitans.tarotapplication.data.TarotCardDBHelper;
import com.thetitans.tarotapplication.model.CardData;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CS on 17-Feb-16.
 */
public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private final int SPLASH_SCREEN_TIME_DELAY = 3050;
    TarotCardDBHelper tarotCardDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        File path = this.getDatabasePath(TarotCardDBHelper.DATABASE_NAME);

        XmlPullParser parser = getResources().getXml(R.xml.data);
        if(path.exists()) {
            //Log.e(TAG, "DB Path : " + path);
            insertData(parser);
            startMainPage();
        }
        else
            Log.e(TAG, "Path not found...");

        /*RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(3000);
        rotate.setInterpolator(new LinearInterpolator());*/

        ImageView image= (ImageView) findViewById(R.id.tarotCover);
        final Animation myRotation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotator);
        image.startAnimation(myRotation);
        //image.startAnimation(rotate);
    }

    public void startMainPage() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_SCREEN_TIME_DELAY);
    }

    public void insertData(XmlPullParser parser) {
        try {
            List<CardData> list = readData(parser);
            for(CardData item : list) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(TarotCardContract.TarotCardEntry.COLUMN_CARD_ID, item.getId());
                contentValues.put(TarotCardContract.TarotCardEntry.COLUMN_CARD_NAME, item.getName());
                contentValues.put(TarotCardContract.TarotCardEntry.COLUMN_CARD_MM_NAME, item.getMmname());
                contentValues.put(TarotCardContract.TarotCardEntry.COLUMN_BUSINESS, item.getBusiness());
                contentValues.put(TarotCardContract.TarotCardEntry.COLUMN_MONEY, item.getMoney());
                contentValues.put(TarotCardContract.TarotCardEntry.COLUMN_EDUCATION, item.getEducation());
                contentValues.put(TarotCardContract.TarotCardEntry.COLUMN_HEALTH, item.getHealth());
                contentValues.put(TarotCardContract.TarotCardEntry.COLUMN_LOVE, item.getLove());
                contentValues.put(TarotCardContract.TarotCardEntry.COLUMN_RANDOM, item.getRandom());
                //Log.e(TAG, "ID : " + item.getId() + "\tName : " + item.getMmname());
                getApplicationContext().getContentResolver().insert(TarotCardContract.TarotCardEntry.CONTENT_URI, contentValues);
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
    }

    public List<CardData> readData(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<CardData> cardLists = new ArrayList();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("card")) {
                cardLists.add(readCard(parser));
            }
        }
        return cardLists;
    }

    private CardData readCard(XmlPullParser parser) throws XmlPullParserException, IOException {

        CardData cardData = new CardData();

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tagName = parser.getName();
            if (tagName.equals("id")) {
                cardData.setId(readText(parser));
            } else if (tagName.equals("name")) {
                cardData.setName(readText(parser));
            } else if (tagName.equals("mmname")) {
                cardData.setMmname(readText(parser));
            } else if (tagName.equals("business")) {
                cardData.setBusiness(readText(parser));
            } else if (tagName.equals("money")) {
                cardData.setMoney(readText(parser));
            } else if (tagName.equals("education")) {
                cardData.setEducation(readText(parser));
            } else if (tagName.equals("love")) {
                cardData.setLove(readText(parser));
            } else if (tagName.equals("health")) {
                cardData.setHealth(readText(parser));
            } else if (tagName.equals("random")) {
                cardData.setRandom(readText(parser));
            }
        }
        return cardData;
    }


    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
}
