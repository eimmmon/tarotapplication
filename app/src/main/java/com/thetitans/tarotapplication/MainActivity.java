package com.thetitans.tarotapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.thetitans.tarotapplication.aboutus.AboutUsActivity;
import com.thetitans.tarotapplication.data.TarotCardDBHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    Context mContext;
    private RelativeLayout actionAries;
    private RelativeLayout actionTaurus;
    private RelativeLayout actionGemini;
    private RelativeLayout actionCancer;
    private RelativeLayout actionLeo;
    private RelativeLayout actionVirgo;
    private RelativeLayout actionLibra;
    private RelativeLayout actionScorpio;
    private RelativeLayout actionSagittarius;
    private RelativeLayout actionCapricorn;
    private RelativeLayout actionAquarius;
    private RelativeLayout actionPisces;

    TarotCardDBHelper tarotCardDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actionAries = (RelativeLayout) findViewById(R.id.action_aries);
        actionTaurus = (RelativeLayout) findViewById(R.id.action_taurus);
        actionGemini = (RelativeLayout) findViewById(R.id.action_gemini);
        actionCancer = (RelativeLayout) findViewById(R.id.action_cancer);
        actionLeo = (RelativeLayout) findViewById(R.id.action_leo);
        actionVirgo = (RelativeLayout) findViewById(R.id.action_virgo);
        actionLibra = (RelativeLayout) findViewById(R.id.action_libra);
        actionScorpio = (RelativeLayout) findViewById(R.id.action_scorpio);
        actionSagittarius = (RelativeLayout) findViewById(R.id.action_sagittarius);
        actionCapricorn = (RelativeLayout) findViewById(R.id.action_capricorn);
        actionAquarius = (RelativeLayout) findViewById(R.id.action_aquarius);
        actionPisces = (RelativeLayout) findViewById(R.id.action_pisces);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionAries.setOnClickListener(this);
        actionTaurus.setOnClickListener(this);
        actionGemini.setOnClickListener(this);
        actionCancer.setOnClickListener(this);
        actionLeo.setOnClickListener(this);
        actionVirgo.setOnClickListener(this);
        actionLibra.setOnClickListener(this);
        actionScorpio.setOnClickListener(this);
        actionSagittarius.setOnClickListener(this);
        actionCapricorn.setOnClickListener(this);
        actionAquarius.setOnClickListener(this);
        actionPisces.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        String res;
        switch (id) {
            case R.id.action_aries :
                res = "Aries";
                click(res);
                break;

            case R.id.action_taurus :
                res = "Taurus";
                click(res);
                break;

            case R.id.action_gemini :
                res = "Gemini";
                click(res);
                break;

            case R.id.action_cancer :
                res = "Cancer";
                click(res);
                break;

            case R.id.action_leo :
                res = "Leo";
                click(res);
                break;

            case R.id.action_virgo :
                res = "Virgo";
                click(res);
                break;

            case R.id.action_libra :
                res = "Libra";
                click(res);
                break;

            case R.id.action_scorpio :
                res = "Scorpio";
                click(res);
                break;

            case R.id.action_sagittarius :
                res = "Sagittarius";
                click(res);
                break;

            case R.id.action_capricorn :
                res = "Capricorn";
                click(res);
                break;

            case R.id.action_aquarius :
                res = "Aquarius";
                click(res);
                break;

            case R.id.action_pisces :
                res = "Pisces";
                click(res);
                break;
        }
    }

    public void click(String str) {
        Log.e("MainActivity.class", "Zodiac -- " + str);
        Intent myIntent = new Intent(this, CategoryListActivity.class);
        Bundle myBundle = new Bundle();
        myBundle.putString("Zodiac", str);
        myIntent.putExtras(myBundle);
        startActivity(myIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_aboutus) {
            Intent intent = new Intent(this, AboutUsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}