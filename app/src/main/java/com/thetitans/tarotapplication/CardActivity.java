package com.thetitans.tarotapplication;

import android.content.Intent;
import android.media.AudioManager;
import android.media.Image;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thetitans.tarotapplication.animation.FlipAnimation;
import com.thetitans.tarotapplication.model.Card;

import java.util.ArrayList;
import java.util.Random;

public class CardActivity extends AppCompatActivity {

    private static final String TAG = CardActivity.class.getSimpleName();
    private final int SPLASH_SCREEN_TIME_DELAY = 1000;
    private final int CARD_TIME_DELAY = 3050;
    RelativeLayout cardBack;
    ImageView image;
    //private ImageView cardImage;
    Card selectedCard;
    // Random object
    private final static Random random = new Random();

    // The card deck
    ArrayList<Card> card = new ArrayList<Card>();

    String categoryResult = "";
    String zodiacResult = "";

    //MediaPlayer player;
    //SoundPool soundPool;
    int shufflingSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadCard();
        Bundle myBundle = getIntent().getExtras();
        if(myBundle != null) {
            if(myBundle.containsKey("Category")) {
                categoryResult = myBundle.getString("Category");
                zodiacResult = myBundle.getString("ZodiacResult");
            }
        }
        setContentView(R.layout.activity_card);
        //soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        //shufflingSound = soundPool.load(getApplicationContext(), R.raw.shuffling_cards, 1);
        cardBack = (RelativeLayout) findViewById(R.id.card_back);
        image= (ImageView) findViewById(R.id.card_tarotCover);
    }

    public void onCardClick(View v) {

        //soundPool.play(shufflingSound, 1.0f, 1.0f, 0, 0, 1.5f);
        cardBack.setClickable(false);
        final Animation myRotation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotator);
        image.startAnimation(myRotation);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                flipCard();
                startDetailPage();
            }
        }, CARD_TIME_DELAY);

    }

    private void flipCard()  {
        View rootLayout = findViewById(R.id.root_view);
        View cardFace = findViewById(R.id.card_face);
        View cardBack = findViewById(R.id.card_back);
        int randomNumber = random.nextInt(card.size());
        selectedCard = card.get(randomNumber);

        cardFace.setBackgroundResource(selectedCard.getCardImg());

        FlipAnimation flipAnimation = new FlipAnimation(cardFace, cardBack);

        if (cardFace.getVisibility() == View.GONE)
        {
            flipAnimation.reverse();
        }
        rootLayout.startAnimation(flipAnimation);
    }

    public void startDetailPage() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(CardActivity.this, CardDetailActivity.class);
                Bundle myBundle = new Bundle();
                myBundle.putString("CategoryResult", categoryResult);
                myBundle.putString("CooseCard", selectedCard.getCardName());
                myBundle.putInt("ChooseCardImg", selectedCard.getCardImg());
                Log.e(TAG, "Category Result : " + categoryResult);
                intent.putExtras(myBundle);
                startActivity(intent);
                finish();
            }
        }, SPLASH_SCREEN_TIME_DELAY);
    }

    public void loadCard() {
        card.add(new Card("Chariot", R.drawable.chariot));
        card.add(new Card("Death", R.drawable.death));
        card.add(new Card("Devil", R.drawable.devil));
        card.add(new Card("Emperess", R.drawable.emperess));
        card.add(new Card("Emperor", R.drawable.emperor));
        card.add(new Card("Fool", R.drawable.fool));
        card.add(new Card("Hanged Man", R.drawable.hanged_man));
        card.add(new Card("Hermit", R.drawable.hermit));
        card.add(new Card("Hierophant", R.drawable.hierophant));
        card.add(new Card("Judgement", R.drawable.judgement));
        card.add(new Card("Justice", R.drawable.justice));
        card.add(new Card("Lovers", R.drawable.lovers));
        card.add(new Card("Magician", R.drawable.magician));
        card.add(new Card("Moon", R.drawable.moon));
        card.add(new Card("Priestess", R.drawable.priestess));
        card.add(new Card("Stars", R.drawable.stars));
        card.add(new Card("Strength", R.drawable.strength));
        card.add(new Card("Sun", R.drawable.sun));
        card.add(new Card("Temperance", R.drawable.temperance));
        card.add(new Card("Tower", R.drawable.tower));
        card.add(new Card("Wheel Of Fortune", R.drawable.wheel_of_fortune));
        card.add(new Card("World", R.drawable.world));
    }
}