package com.thetitans.tarotapplication;

import android.content.ContentValues;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;
import android.database.Cursor;
import com.thetitans.tarotapplication.data.TarotCardContract;
import com.thetitans.tarotapplication.data.TarotCardDBHelper;
import com.thetitans.tarotapplication.model.CardData;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CS on 11-Feb-16.
 */
public class TestDb extends AndroidTestCase {

    public static final String LOG_TAG = TestDb.class.getSimpleName();

    public void testCreateDb() throws Throwable {
        mContext.deleteDatabase(TarotCardDBHelper.DATABASE_NAME);
        SQLiteDatabase db = new TarotCardDBHelper(this.mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());
        db.close();
    }

    public void testInsertReadDb() {

        TarotCardDBHelper dbHelper = new TarotCardDBHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        /*String testCardId = "1";
        String testCardName = "Empress";
        String testCardMMName = "ဧကရီ";
        String testBusiness = "•\tအလုပ္အကိုင္မ်ားတိုးတက္ေအာင္ျမင္မည္။\n" +
                "•\tအၾကီးအကဲအထက္လူၾကီးကူညီမည္။\n" +
                "•\tလုပ္ငန္းအတြင္း ေကာင္းေသာေျပာင္းလဲမွဳမ်ားၾကံဳမည္။\n" +
                "•\tအခက္အခဲမ်ားေက်ာ္လြားနိုင္မည္။\n";
        String testMoney = "•\tရုတ္တရက္ေငြရကိန္းရွိသည္။\n" +
                "•\tအဆံုးတြက္ထားေသာ ေငြေၾကးပစၥည္းမ်ားျပန္ရမည္။\n" +
                "•\tေငြေၾကးနွင့္ပါတ္သက္၍ေျပာစကားေအာင္ျမင္မည္။\n";
        String testEducation = "•\tပညာေရးအထူးေကာင္းေသာကာလျဖစ္သည္။\n" +
                "•\tဆရာသမားေကာင္းနွင့္ေတြ႕ၾကံဳရမည္။\n" +
                "•\tထင္ရွားေလာက္ေသာ ေအာင္ျမင္မွဳမ်ားရမည္။\n" +
                "•\tထူးျခားေသာ အတတ္ပညာမ်ားသင္ယူရမည္။\n";
        String testHealth = "•\tက်န္းမာေရးပိုေကာင္းလာမည္။\n" +
                "•\tမ်က္စိ ႏွင့္ ဝမ္းဗိုက္ သတိထားေပးပါ။\n" +
                "•\tေသြးေပါင္ခ်ိန္မၾကာခဏခ်ိန္ေပးပါ။\n";
        String testLove = "•\tအခ်စ္ေရးအထူးေကာင္းသည္။\n" +
                "•\tခ်စ္သူဝိုင္းဝိုင္းလည္ေနမည္။\n" +
                "•\tခ်စ္သူ အသစ္ေတြ႕မည္။\n" +
                "•\tအိမ္ေထာင္ရွိသူမ်ား ကေလးရမည္။\n";
        String testRandom = "•\tအျခားသူမ်ားဧ။္ ေလးစားျခင္းခံရမည္။\n" +
                "•\tမိတ္ေဆြေကာင္းေတြ႕မည္။\n" +
                "•\tဆံုးျဖတ္ခ်က္မ်ားခ်စရာရွိပါက ရဲရဲသာဆံုးျဖတ္လိုက္ပါ။\n";



        ContentValues values = new ContentValues();
        values.put(TarotCardContract.TarotCardEntry.COLUMN_CARD_ID, testCardId);
        values.put(TarotCardContract.TarotCardEntry.COLUMN_CARD_NAME, testCardName);
        values.put(TarotCardContract.TarotCardEntry.COLUMN_CARD_MM_NAME, testCardMMName);
        values.put(TarotCardContract.TarotCardEntry.COLUMN_BUSINESS, testBusiness);
        values.put(TarotCardContract.TarotCardEntry.COLUMN_MONEY, testMoney);
        values.put(TarotCardContract.TarotCardEntry.COLUMN_EDUCATION, testEducation);
        values.put(TarotCardContract.TarotCardEntry.COLUMN_HEALTH, testHealth);
        values.put(TarotCardContract.TarotCardEntry.COLUMN_LOVE, testLove);
        values.put(TarotCardContract.TarotCardEntry.COLUMN_RANDOM, testRandom);

        long tarotCardRowId;
        tarotCardRowId = db.insert(TarotCardContract.TarotCardEntry.TABLE_NAME, null, values);

        assertTrue(tarotCardRowId != -1);
        Log.d(LOG_TAG, "New row id: " + tarotCardRowId);*/

        // Specify which columns you want
        String[] columns = {
                TarotCardContract.TarotCardEntry.COLUMN_CARD_ID,
                TarotCardContract.TarotCardEntry.COLUMN_CARD_NAME,
                TarotCardContract.TarotCardEntry.COLUMN_CARD_MM_NAME,
                TarotCardContract.TarotCardEntry.COLUMN_BUSINESS,
                TarotCardContract.TarotCardEntry.COLUMN_MONEY,
                TarotCardContract.TarotCardEntry.COLUMN_EDUCATION,
                TarotCardContract.TarotCardEntry.COLUMN_HEALTH,
                TarotCardContract.TarotCardEntry.COLUMN_LOVE
        };

        // A cursor is your primary interface to the query results.
        Cursor cursor = db.query(
                TarotCardContract.TarotCardEntry.TABLE_NAME, //Table to Query
                columns,
                null, // Columns for the "where" clause
                null, // Values for the "where" clause
                null, // columns to group by
                null, // columns to filter by row groups
                null // sort order
        );

        // If possible, move to the first row of the query results.
        if(cursor.moveToFirst()) {
            int cardIdIndex = cursor.getColumnIndex(TarotCardContract.TarotCardEntry.COLUMN_CARD_ID);
            String cardId = cursor.getString(cardIdIndex);

            int cardNameIndex = cursor.getColumnIndex(TarotCardContract.TarotCardEntry.COLUMN_CARD_NAME);
            String cardName = cursor.getString(cardNameIndex);

            int cardMMNameIndex = cursor.getColumnIndex(TarotCardContract.TarotCardEntry.COLUMN_CARD_MM_NAME);
            String cardMMName = cursor.getString(cardMMNameIndex);

            int businessIndex = cursor.getColumnIndex(TarotCardContract.TarotCardEntry.COLUMN_BUSINESS);
            String business = cursor.getString(businessIndex);

            int moneyIndex = cursor.getColumnIndex(TarotCardContract.TarotCardEntry.COLUMN_MONEY);
            String money = cursor.getString(moneyIndex);

            int educationIndex = cursor.getColumnIndex(TarotCardContract.TarotCardEntry.COLUMN_EDUCATION);
            String education = cursor.getString(educationIndex);

            int healthIndex = cursor.getColumnIndex(TarotCardContract.TarotCardEntry.COLUMN_HEALTH);
            String health = cursor.getString(healthIndex);

            int loveIndex = cursor.getColumnIndex(TarotCardContract.TarotCardEntry.COLUMN_LOVE);
            String love = cursor.getString(loveIndex);

            /*assertEquals(testCardId, cardId);
            assertEquals(testCardName, cardName);
            assertEquals(testCardMMName, cardMMName);
            assertEquals(testBusiness, business);
            assertEquals(testMoney, money);
            assertEquals(testEducation, education);
            assertEquals(testHealth, health);
            assertEquals(testLove, love);*/
        } else {
            fail("No values returned :(");
        }

        dbHelper.close();
    }
}
